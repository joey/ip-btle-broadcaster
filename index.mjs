import os from "os"

import findMyIp from "./lib/find-my-ip.mjs"
import tologs from "./lib/tologs.mjs"
import MakeReceiver from "./lib/receiver.mjs"
import {MakeObservable} from "./lib/observable.mjs"
import MakeBroadcaster from "./lib/broadcaster.mjs"

let otherMachineIp = MakeObservable({address: null})
const primaryServiceUUID = "ec00"
const characteristicUUID = "ec0e"
let has = {
  received: false,
  broadcasted: false
}

let myip = {address: "not found yet"}

otherMachineIp.observe("address", {update: (key, old, value)=>{
  tologs(`Other machine ip address is ${value}`)
}})

findMyIp(os.networkInterfaces()).then( iface => {
  myip = iface
}).catch( faces => {
  tologs(`Something went wrong when trying to query this machines network ineteraces ${faces}`)
})

const broadcaster = MakeBroadcaster(primaryServiceUUID, characteristicUUID, {
  shouldSend(){
    return myip !== null
  },
  createMessageToSend(){
    return myip.address
  },
  done(){
    has.broadcasted = true
  }
})

const receiver = MakeReceiver(primaryServiceUUID, characteristicUUID, {
  received(address) {
    otherMachineIp.address = address
    has.received = true
  }
})

let interval = setInterval(()=>{
  if(has.received && has.broadcasted){
    clearInterval(interval)
    process.exit()
  }
}, 3000)

process.stdin.resume()
function exitHandler() {
  tologs(arguments)
  receiver.stopAdvertising()
  process.exit()
}
["exit", "SIGINT", "SIGUSR1", "uncaughtException"].forEach( key => {
  process.on(key, exitHandler)
})

setTimeout(()=>{
  process.exit()
}, 60000)