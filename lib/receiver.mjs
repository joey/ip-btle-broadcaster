import bleno from "bleno"
import tologs from "./tologs.mjs"

const MakeReceiver = (primaryServiceUUID, characteristicUUID, delegate) => {
    tologs("starting peripheral")
    let handler = {
        stateChanged(state){
            tologs(`stateChange ${state}`)
            if (state === "poweredOn") return bleno.startAdvertising('echo', ['ec00'])
            bleno.stopAdvertising()
        },
        startErrored(err){
            console.error(err)
        },
        startedAdvertising(err){
            tologs(`advertisingStart ${(err ? "errored occured" + err : "succeeded")}`)
            if(err) return
            let echo = new bleno.Characteristic({
                uuid: characteristicUUID,
                properties: ["read", "write", "notify"],
                onWriteRequest: (data, offset, withoutResponse, callback) => {
                    delegate.received(data.toString())
                }
            })
            bleno.setServices([
                new bleno.PrimaryService({
                    uuid: primaryServiceUUID,
                    characteristics: [echo]
                })
            ])
        }
    }
    bleno.on("stateChange", handler.stateChanged)
    bleno.on("advertisingStartError", handler.startErrored)
    bleno.on("advertisingStart", handler.startedAdvertising)
    return bleno
}

export default MakeReceiver