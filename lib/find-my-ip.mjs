let findMyIp = (ifaces) => {
  return new Promise((resolve, reject) => {
    let wasFound = false
    Object.keys(ifaces).forEach( key => {
      ifaces[key].forEach( face => {
        if(!face.internal && face.family === "IPv4"){
          wasFound = true
          resolve(face)
        }
      })
    })
    if(!wasFound) reject(ifaces)
  })
}

export default findMyIp