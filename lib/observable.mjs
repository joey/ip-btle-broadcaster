const MakeObservable = obj => {
  let observers = {}
  let self = {
    observe(key, observer){
      if(!observers[key]) observers[key] = []
      observers[key].push(observer)
    },
    changed(key, old, value){
      if(!observers[key]) return
      observers[key].forEach( o => {
        o.update(key, old, value, this)
      })
    }
  }

  let keys = Object.keys(obj)
  let ubounds = keys.length
  for(let i = 0; i < ubounds; i++){
    (() => {
      let prop = keys[i]
      Reflect.defineProperty(self, prop, {
        get(){
          return obj[prop]
        },
        set(v){
          let old = obj[prop]
          obj[prop] = v
          self.changed(prop, old, v)
        },
        enumerable: true
      })
    })()
  }
  return self
}


const MakeObservableList = list =>{
  let observable = MakeObservable({})

  let self = {
    push(item){
      list.push(item)
      observable.changed("push", null, item)
    },
    list: list,
    pop(){
      let last = list.pop()
      observable.changed("pop", last, null)
      return last
    },
    shift(){
      let first = list.shift()
      observable.changed("shift", null, first)
      return first
    },
    unshift(items){
      let length = list.unshift(items)
      observable.changed("unshift", null, items)
      return length
    },
    remove(delegate){
      let i = 0
      let ubounds = list.length
      let deleted = []
      for(i; i<ubounds;i++){
        if(delegate(i, list[i])){
          deleted = list.splice(i, 1)
          observable.changed("remove", deleted[0], i)
          break
        }
      }
      return deleted[0]
    },
    removeMany(delegate){
      let ubounds = list.length
      let i = ubounds-1
      let deleted = []
      for(i;i>=0;i--){
        if(list[i] && delegate(i, list[i])){
          deleted.push(list.splice(i, 1)[0])
          observable.changed("remove", deleted[deleted.length-1], i)
        }
      }
      return deleted
    },
    item(i){
      return list[i]
    },
    find(delegate){
      let i = 0
      let ubounds = list.length
      for(i; i<ubounds;i++){
        if(delegate(i, list[i])) return list[i]
      }
      return null
    },
    each(fn){
      let i = 0
      let ubounds = list.length
      for(i; i < ubounds; i++){
        fn(i, list[i])
      }
    },
    clear(){
      while(this.length > 0) this.pop()
      list = []
    },
    release(){
      let item = null
      while(item = list.pop()){}
      observable.release()
    },
    observe(key, observer){
      observable.observe(key, observer)
    },
    stopObserving(observer){
      observable.stopObserving(observer)
    }
  }
  Reflect.defineProperty(self, "length", {
    get(){
      return list.length
    },
    enumerable: true
  })
  return self
}

export {MakeObservable, MakeObservableList}