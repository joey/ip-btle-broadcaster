import noble from "noble"
import {MakeObservableList} from "./observable.mjs"
import tologs from "./tologs.mjs"

let services = MakeObservableList([])
let characteristics = MakeObservableList([])
let peripheral = null
let interval = null

const MakeBroadcaster = (primaryServiceUUID, characteristicUUID, delegate) => {
    const handler = {
        stopScanning(n){
            n.stopScanning()
            tologs("Stopped scanning.")
        },
        poweredOnOff(state){
            if (state === "poweredOn") {
                tologs("Start scanning...")
                return noble.startScanning([primaryServiceUUID])
            }
            this.stopScanning(noble)
        },
        discovered(p){
            tologs("Discovered.")
            tologs(`Connecting to ${p.advertisement.localName} ${p.id}.`)
            peripheral = p
            this.stopScanning(noble)
            p.on("connect", this.connected.bind(this))
            p.on("disconnect", this.disconnected.bind(this))
            p.connect()        
        },
        connected(err){
            tologs(`Connected ${peripheral.advertisement.localName}.`)
            peripheral.discoverSomeServicesAndCharacteristics([primaryServiceUUID],
              [characteristicUUID],
              (err, listOfServices, listOfCharacteristics) => {
                tologs(`${listOfServices.length} services and ${listOfCharacteristics.length} characteristics discovered.`)
                if(err) return tologs(err)
                if(listOfServices.length > 0){
                    listOfServices.forEach( s => {
                        services.remove(service => {
                            return service.uuid === s.uuid
                        })
                    })
                    listOfServices.forEach( s => {
                        services.push(s)
                    })
                }
          
                if(listOfCharacteristics.length > 0){
                    listOfCharacteristics.forEach( c =>{
                        characteristics.remove( characteristic => {
                            return characteristic.uuid === c.uuid
                        })
                    })
          
                    listOfCharacteristics.forEach( c=> {
                        characteristics.push(c)
                    })
                }
            })
        },
        disconnected(){
            tologs("Disconnected.")
        },
        interval: interval
    }
    noble.on("stateChange", handler.poweredOnOff.bind(handler))
    noble.on("discover", handler.discovered.bind(handler))
    characteristics.observe("push", {update(key, old, value){
        value.on("data", d=>{tologs(`new char${d}`)})
    }})

    interval = setInterval(() => {
        if(delegate.shouldSend()){
            const message = Buffer.from(delegate.createMessageToSend(), "utf-8")
            tologs(`Sending ${message}, ${services.length}`)
            let counter = 0
            characteristics.each((i, c)=>{
                counter++
                c.write(message)
            })
            if(counter > 0) {
                clearInterval(interval)
                delegate.done()
            }
        }
    }, 2000)
    return noble  
}


export default MakeBroadcaster