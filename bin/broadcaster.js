#!/usr/bin/env node

const os = require("os")
const findMyIp = (ifaces) => {
  return new Promise((resolve, reject) => {
    let wasFound = false
    Object.keys(ifaces).forEach( key => {
      ifaces[key].forEach( face => {
        if(!face.internal && face.family === "IPv4"){
          wasFound = true
          resolve(face)
        }
      })
    })
    if(!wasFound) reject(ifaces)
  })
}

const tologs = message => {
  console.log("[bleno]", message)
}

const bleno = require("bleno")
const MakeReceiver = (primaryServiceUUID, characteristicUUID, delegate) => {
    tologs("starting peripheral")
    let handler = {
        stateChanged(state){
            tologs(`stateChange ${state}`)
            if (state === "poweredOn") return bleno.startAdvertising('echo', ['ec00'])
            bleno.stopAdvertising()
        },
        startErrored(err){
            console.error(err)
        },
        startedAdvertising(err){
            tologs(`advertisingStart ${(err ? "errored occured" + err : "succeeded")}`)
            if(err) return
            let echo = new bleno.Characteristic({
                uuid: characteristicUUID,
                properties: ["read", "write", "notify"],
                onWriteRequest: (data, offset, withoutResponse, callback) => {
                    delegate.received(data.toString())
                }
            })
            bleno.setServices([
                new bleno.PrimaryService({
                    uuid: primaryServiceUUID,
                    characteristics: [echo]
                })
            ])
        }
    }
    bleno.on("stateChange", handler.stateChanged)
    bleno.on("advertisingStartError", handler.startErrored)
    bleno.on("advertisingStart", handler.startedAdvertising)
    return bleno
}

const MakeObservable = obj => {
  let observers = {}
  let self = {
    observe(key, observer){
      if(!observers[key]) observers[key] = []
      observers[key].push(observer)
    },
    changed(key, old, value){
      if(!observers[key]) return
      observers[key].forEach( o => {
        o.update(key, old, value, this)
      })
    }
  }

  let keys = Object.keys(obj)
  let ubounds = keys.length
  for(let i = 0; i < ubounds; i++){
    (() => {
      let prop = keys[i]
      Reflect.defineProperty(self, prop, {
        get(){
          return obj[prop]
        },
        set(v){
          let old = obj[prop]
          obj[prop] = v
          self.changed(prop, old, v)
        },
        enumerable: true
      })
    })()
  }
  return self
}

const MakeObservableList = list =>{
  let observable = MakeObservable({})

  let self = {
    push(item){
      list.push(item)
      observable.changed("push", null, item)
    },
    list: list,
    pop(){
      let last = list.pop()
      observable.changed("pop", last, null)
      return last
    },
    shift(){
      let first = list.shift()
      observable.changed("shift", null, first)
      return first
    },
    unshift(items){
      let length = list.unshift(items)
      observable.changed("unshift", null, items)
      return length
    },
    remove(delegate){
      let i = 0
      let ubounds = list.length
      let deleted = []
      for(i; i<ubounds;i++){
        if(delegate(i, list[i])){
          deleted = list.splice(i, 1)
          observable.changed("remove", deleted[0], i)
          break
        }
      }
      return deleted[0]
    },
    removeMany(delegate){
      let ubounds = list.length
      let i = ubounds-1
      let deleted = []
      for(i;i>=0;i--){
        if(list[i] && delegate(i, list[i])){
          deleted.push(list.splice(i, 1)[0])
          observable.changed("remove", deleted[deleted.length-1], i)
        }
      }
      return deleted
    },
    item(i){
      return list[i]
    },
    find(delegate){
      let i = 0
      let ubounds = list.length
      for(i; i<ubounds;i++){
        if(delegate(i, list[i])) return list[i]
      }
      return null
    },
    each(fn){
      let i = 0
      let ubounds = list.length
      for(i; i < ubounds; i++){
        fn(i, list[i])
      }
    },
    clear(){
      while(this.length > 0) this.pop()
      list = []
    },
    release(){
      let item = null
      while(item = list.pop()){}
      observable.release()
    },
    observe(key, observer){
      observable.observe(key, observer)
    },
    stopObserving(observer){
      observable.stopObserving(observer)
    }
  }
  Reflect.defineProperty(self, "length", {
    get(){
      return list.length
    },
    enumerable: true
  })
  return self
}

const noble = require("noble")
let services = MakeObservableList([])
let characteristics = MakeObservableList([])
let peripheral = null
let interval = null
const MakeBroadcaster = (primaryServiceUUID, characteristicUUID, delegate) => {
    const handler = {
        stopScanning(n){
            n.stopScanning()
            tologs("Stopped scanning.")
        },
        poweredOnOff(state){
            if (state === "poweredOn") {
                tologs("Start scanning...")
                return noble.startScanning([primaryServiceUUID])
            }
            this.stopScanning(noble)
        },
        discovered(p){
            tologs("Discovered.")
            tologs(`Connecting to ${p.advertisement.localName} ${p.id}.`)
            peripheral = p
            this.stopScanning(noble)
            p.on("connect", this.connected.bind(this))
            p.on("disconnect", this.disconnected.bind(this))
            p.connect()        
        },
        connected(err){
            tologs(`Connected ${peripheral.advertisement.localName}.`)
            peripheral.discoverSomeServicesAndCharacteristics([primaryServiceUUID],
              [characteristicUUID],
              (err, listOfServices, listOfCharacteristics) => {
                tologs(`${listOfServices.length} services and ${listOfCharacteristics.length} characteristics discovered.`)
                if(err) return tologs(err)
                if(listOfServices.length > 0){
                    listOfServices.forEach( s => {
                        services.remove(service => {
                            return service.uuid === s.uuid
                        })
                    })
                    listOfServices.forEach( s => {
                        services.push(s)
                    })
                }
          
                if(listOfCharacteristics.length > 0){
                    listOfCharacteristics.forEach( c =>{
                        characteristics.remove( characteristic => {
                            return characteristic.uuid === c.uuid
                        })
                    })
          
                    listOfCharacteristics.forEach( c=> {
                        characteristics.push(c)
                    })
                }
            })
        },
        disconnected(){
            tologs("Disconnected.")
        },
        interval: interval
    }
    noble.on("stateChange", handler.poweredOnOff.bind(handler))
    noble.on("discover", handler.discovered.bind(handler))
    characteristics.observe("push", {update(key, old, value){
        value.on("data", d=>{tologs(`new char${d}`)})
    }})

    interval = setInterval(() => {
        if(delegate.shouldSend()){
            const message = Buffer.from(delegate.createMessageToSend(), "utf-8")
            tologs(`Sending ${message}, ${services.length}`)
            let counter = 0
            characteristics.each((i, c)=>{
                counter++
                c.write(message)
            })
            if(counter > 0) {
                clearInterval(interval)
                delegate.done()
            }
        }
    }, 2000)
    return noble  
}

let otherMachineIp = MakeObservable({address: null})
const primaryServiceUUID = "ec00"
const characteristicUUID = "ec0e"
let has = {
  received: false,
  broadcasted: false
}

let myip = {address: "not found yet"}

otherMachineIp.observe("address", {update: (key, old, value)=>{
  tologs(`Other machine ip address is ${value}`)
}})

findMyIp(os.networkInterfaces()).then( iface => {
  myip = iface
}).catch( faces => {
  tologs(`Something went wrong when trying to query this machines network ineteraces ${faces}`)
})

const broadcaster = MakeBroadcaster(primaryServiceUUID, characteristicUUID, {
  shouldSend(){
    return myip !== null
  },
  createMessageToSend(){
    return myip.address
  },
  done(){
    has.broadcasted = true
  }
})

const receiver = MakeReceiver(primaryServiceUUID, characteristicUUID, {
  received(address) {
    otherMachineIp.address = address
    has.received = true
  }
})

let watchingInterval = setInterval(()=>{
  if(has.received && has.broadcasted){
    clearInterval(watchingInterval)
    process.exit()
  }
}, 3000)

process.stdin.resume()
function exitHandler() {
  tologs(arguments)
  receiver.stopAdvertising()
  process.exit()
}
["exit", "SIGINT", "SIGUSR1", "uncaughtException"].forEach( key => {
  process.on(key, exitHandler)
})

setTimeout(()=>{
  process.exit()
}, 60000)